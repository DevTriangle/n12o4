package com.triangle.n12o4.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.triangle.n12o4.R

val LatoFontFamily = FontFamily(
    Font(R.font.lato_bold, FontWeight.Bold)
)

val SourceSansPro = FontFamily(
    Font(R.font.ss_pro_regular, FontWeight.Normal),
    Font(R.font.ss_pro_semibold, FontWeight.SemiBold),
    Font(R.font.ss_pro_bold, FontWeight.Bold),
)

val Typography = Typography(
    body1 = TextStyle(
        fontFamily = SourceSansPro,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    )
    /* Other default text styles to override
    button = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.W500,
        fontSize = 14.sp
    ),
    caption = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp
    )
    */
)