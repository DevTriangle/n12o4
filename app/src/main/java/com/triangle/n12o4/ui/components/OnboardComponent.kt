package com.triangle.n12o4.ui.components

import android.widget.Space
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.triangle.n12o4.ui.theme.LatoFontFamily
import com.triangle.n12o4.ui.theme.captionColor
import com.triangle.n12o4.ui.theme.onboardTitle
import com.triangle.n12o4.ui.theme.primaryVariant

/*
Описание: Компонент приветственного экрана
Дата создания: 29.03.2023
Автор: Хасанов Альберт
*/
@Composable
fun OnboardComponent(
    currentIndex: Int,
    info: Map<String, Any>
) {
    Column(
        modifier = Modifier
            .padding(top = 60.dp, bottom = 55.dp, start = 20.dp, end = 20.dp)
            .fillMaxSize(),
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(modifier = Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally) {
            Column(modifier = Modifier
                .widthIn(max = 220.dp)
                .fillMaxWidth()
                .height(120.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = info["title"].toString(),
                    fontFamily = LatoFontFamily,
                    fontSize = 20.sp,
                    color = onboardTitle
                )
                Spacer(modifier = Modifier.height(29.dp))
                Text(
                    text = info["desc"].toString(),
                    fontSize = 14.sp,
                    color = captionColor
                )
            }
            Spacer(modifier = Modifier.height(40.dp))
            DotsIndicator(count = 3, currentIndex = currentIndex)
        }
        Image(
            painter = info["image"] as Painter,
            contentDescription = "",
            modifier = Modifier
                .height(270.dp),
            contentScale = ContentScale.FillHeight
        )
    }
}
/*
Описание: Индикатор номера экрана
Дата создания: 29.03.2023
Автор: Хасанов Альберт
*/
@Composable
fun DotsIndicator(
    count: Int,
    currentIndex: Int
) {
    Row() {
        for (i in 0 until count) {
            Box(modifier = Modifier
                .padding(4.dp)
                .size(14.dp)
                .clip(CircleShape)
                .border(1.dp, primaryVariant, CircleShape)
                .background(if (currentIndex == i) primaryVariant else Color.White)
            )
        }
    }
}