package com.triangle.n12o4.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import com.triangle.n12o4.ui.theme.N12o4Theme
import com.triangle.n12o4.R
import com.triangle.n12o4.ui.components.AppButton
import com.triangle.n12o4.ui.components.AppTextButton
import com.triangle.n12o4.ui.components.AppTextField
import com.triangle.n12o4.ui.components.LoadingDialog
import com.triangle.n12o4.ui.theme.captionColor
import com.triangle.n12o4.ui.theme.description
import com.triangle.n12o4.ui.theme.inputStroke
import com.triangle.n12o4.viewmodel.LoginViewModel

/*
Описание: Активити экрана авторизации
Дата создания: 29.03.2023
Автор: Хасанов Альберт
*/
class LoginActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            N12o4Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Содержание экрана авторизации
    Дата создания: 29.03.2023
    Автор: Хасанов Альберт
    */
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current
        val viewModel = ViewModelProvider(this)[LoginViewModel::class.java]

        var email by rememberSaveable { mutableStateOf("") }

        var isLoading by rememberSaveable { mutableStateOf(false) }
        var isErrorVisible by rememberSaveable { mutableStateOf(false) }

        val isSuccess by viewModel.isSuccess.observeAsState()
        LaunchedEffect(isSuccess) {
            if (isSuccess == true) {
                isLoading = false

                val intent = Intent(mContext, CodeActivity::class.java)
                intent.putExtra("email", email)
                startActivity(intent)
            }
        }

        val errorMessage by viewModel.message.observeAsState()
        LaunchedEffect(errorMessage) {
            if (errorMessage != null) {
                isLoading = false
                isErrorVisible = true
            }
        }

        Column(
            modifier = Modifier
                .widthIn(max = 400.dp)
                .fillMaxWidth()
                .padding(20.dp),
            verticalArrangement = Arrangement.SpaceBetween,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Column(
                modifier = Modifier
                    .widthIn(max = 400.dp)
                    .fillMaxWidth(),
            ) {
                Spacer(modifier = Modifier.height(60.dp))
                Row(
                    modifier = Modifier
                        .widthIn(max = 400.dp)
                        .fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Image(
                        painter = painterResource(
                            id = R.drawable.ic_hello
                        ),
                        contentDescription = "",
                        modifier = Modifier
                            .size(32.dp)
                    )
                    Spacer(modifier = Modifier.width(16.dp))
                    Text(text = "Добро пожаловать!", fontSize = 24.sp, fontWeight = FontWeight.Bold)
                }
                Spacer(modifier = Modifier.height(23.dp))
                Text(
                    text = "Войдите, чтобы пользоваться функциями приложения",
                    fontSize = 15.sp
                )
                Spacer(modifier = Modifier.height(60.dp))
                AppTextField(
                    value = email,
                    onValueChange = { email = it },
                    modifier = Modifier.fillMaxWidth(),
                    label = {
                        Text(
                            text = "Вход по E-mail",
                            fontSize = 14.sp,
                            color = description
                        )
                    },
                    placeholder = {
                        Text(
                            text = "example@mail.ru",
                            fontSize = 15.sp,
                            color = Color.Black.copy(0.5f)
                        )
                    }
                )
                Spacer(modifier = Modifier.height(32.dp))
                AppButton(
                    onClick = {
                        if (Regex("^[a-zA-Z0-9]*@[a-zA-Z0-9]*\\.[a-zA-Z0-9]{2,}$").matches(email)) {
                            isLoading = true
                            viewModel.sendCode(email)
                        } else {
                            Toast.makeText(mContext, "Неверный формат E-Mail", Toast.LENGTH_LONG).show()
                        }
                    },
                    label = "Далее"
                )
            }
            Column(modifier = Modifier
                .padding(top = 30.dp)
                .widthIn(max = 400.dp)
                .fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally) {
                Text(
                    text = "Или войдите с помощью",
                    fontSize = 15.sp,
                    color = captionColor
                )
                Spacer(modifier = Modifier.height(16.dp))
                AppButton(
                    onClick = {},
                    label = "Войти с Яндекс",
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = Color.White
                    ),
                    border = BorderStroke(1.dp, inputStroke)
                )
            }
        }

        if (isLoading) {
            LoadingDialog()
        }

        if (isErrorVisible) {
            AlertDialog(
                onDismissRequest = { isErrorVisible = false },
                title = {
                    Text(text = "Ошибка", fontWeight = FontWeight.SemiBold, fontSize = 18.sp)
                },
                text = {
                    Text(text = errorMessage.toString(), fontSize = 16.sp)
                },
                buttons = {
                    AppTextButton(
                        onClick = { isErrorVisible = false },
                        label = "Ок"
                    )
                }
            )
        }
    }
}