package com.triangle.n12o4.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState
import com.triangle.n12o4.R
import com.triangle.n12o4.ui.components.AppTextButton
import com.triangle.n12o4.ui.components.OnboardComponent
import com.triangle.n12o4.ui.theme.N12o4Theme
import kotlinx.coroutines.flow.collect

/*
Описание: Активити приветственного экрана
Дата создания: 29.03.2023
Автор: Хасанов Альберт
*/
class OnboardActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            N12o4Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Содержание приветственного экрана
    Дата создания: 29.03.2023
    Автор: Хасанов Альберт
    */
    @OptIn(ExperimentalPagerApi::class)
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current
        val sharedPreferences = this.getSharedPreferences("shared", MODE_PRIVATE)

        val screenList = listOf(
            mapOf(
                "title" to "Анализы",
                "desc" to "Экспресс сбор и получение проб",
                "image" to painterResource(id = R.drawable.onboard_1)
            ),
            mapOf(
                "title" to "Уведомления",
                "desc" to "Вы быстро узнаете о результатах",
                "image" to painterResource(id = R.drawable.onboard_2)
            ),
            mapOf(
                "title" to "Мониторинг",
                "desc" to "Наши врачи всегда наблюдают за вашими показателями здоровья",
                "image" to painterResource(id = R.drawable.onboard_3)
            ),
        )

        val pagerState = rememberPagerState()

        var skipText by rememberSaveable { mutableStateOf("") }

        LaunchedEffect(pagerState) {
            snapshotFlow { pagerState.currentPage }.collect() {
                skipText = if (it < 2) {
                    "Пропустить"
                } else "Завершить"
            }
        }

        Column(modifier = Modifier.fillMaxSize()) {
            Row(modifier = Modifier.fillMaxWidth().padding(top = 5.dp), horizontalArrangement = Arrangement.SpaceBetween) {
                AppTextButton(
                    onClick = {
                        with(sharedPreferences.edit()) {
                            putBoolean("isFirstLaunch", false)
                            apply()
                        }

                        val intent = Intent(mContext, LoginActivity::class.java)
                        startActivity(intent)
                    },
                    label = skipText,
                    modifier = Modifier
                        .padding(start = 30.dp)
                )
                Image(
                    painter = painterResource(id = R.drawable.onboard_logo),
                    contentDescription = ""
                )
            }
            HorizontalPager(count = 3, state = pagerState) { page ->
                OnboardComponent(currentIndex = page, info = screenList[page])
            }
        }
    }
}