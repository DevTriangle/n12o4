package com.triangle.n12o4.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Space
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import com.triangle.n12o4.R
import com.triangle.n12o4.ui.components.AppTextButton
import com.triangle.n12o4.ui.components.AppTextField
import com.triangle.n12o4.ui.components.LoadingDialog
import com.triangle.n12o4.ui.theme.N12o4Theme
import com.triangle.n12o4.ui.theme.captionColor
import com.triangle.n12o4.ui.theme.description
import com.triangle.n12o4.ui.theme.inputBG
import com.triangle.n12o4.viewmodel.LoginViewModel
import kotlinx.coroutines.delay

/*
Описание: Активити экрана ввода кода из email
Дата создания: 29.03.2023
Автор: Хасанов Альберт
*/
class CodeActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            N12o4Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Содержание экрана ввода кода из email
    Дата создания: 29.03.2023
    Автор: Хасанов Альберт
    */
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current
        val viewModel = ViewModelProvider(this)[LoginViewModel::class.java]
        val sharedPreferences = this.getSharedPreferences("shared", MODE_PRIVATE)

        val email = intent.getStringExtra("email")
        var timer by rememberSaveable { mutableStateOf(60) }

        var code1 by rememberSaveable { mutableStateOf("") }
        var code2 by rememberSaveable { mutableStateOf("") }
        var code3 by rememberSaveable { mutableStateOf("") }
        var code4 by rememberSaveable { mutableStateOf("") }

        LaunchedEffect(timer) {
            delay(1000)

            if (timer > 0) {
                timer--
            } else {
                viewModel.sendCode(email!!)
                timer = 60
            }
        }

        var isLoading by rememberSaveable { mutableStateOf(false) }
        var isErrorVisible by rememberSaveable { mutableStateOf(false) }

        val token by viewModel.token.observeAsState()
        LaunchedEffect(token) {
            if (token != null) {
                isLoading = false

                with(sharedPreferences.edit()) {
                    putString("token", token!!)
                    apply()
                }

                val intent = Intent(mContext, PasswordActivity::class.java)
                startActivity(intent)
            }
        }

        val errorMessage by viewModel.message.observeAsState()
        LaunchedEffect(errorMessage) {
            if (errorMessage != null) {
                isLoading = false
                isErrorVisible = true
            }
        }

        Scaffold(topBar = {
            Box(
                modifier = Modifier
                    .padding(20.dp)
                    .size(32.dp)
                    .clip(MaterialTheme.shapes.medium)
                    .background(inputBG)
                    .clickable { onBackPressed() }
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_back),
                    contentDescription = "",
                    modifier = Modifier
                        .align(Alignment.Center),
                    tint = description
                )
            }
        }) {
            Box(modifier = Modifier.padding(it)) {
                Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                        modifier = Modifier
                            .widthIn(max = 400.dp)
                            .fillMaxSize()
                    ) {
                        Text(
                            text = "Введите код из E-mail",
                            fontSize = 17.sp,
                            fontWeight = FontWeight.SemiBold
                        )
                        Spacer(modifier = Modifier.height(24.dp))
                        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
                            AppTextField(
                                value = code1,
                                onValueChange = { code ->
                                    if (code.length < 2) code1 = code
                                },
                                modifier = Modifier
                                    .padding(end = 16.dp)
                                    .size(48.dp),
                                contentPadding = PaddingValues(10.dp),
                                textStyle = TextStyle(
                                    fontSize = 20.sp,
                                    textAlign = TextAlign.Center
                                )
                            )
                            AppTextField(
                                value = code2,
                                onValueChange = { code ->
                                    if (code.length < 2) code2 = code
                                },
                                modifier = Modifier
                                    .padding(end = 16.dp)
                                    .size(48.dp),
                                contentPadding = PaddingValues(10.dp),
                                textStyle = TextStyle(
                                    fontSize = 20.sp,
                                    textAlign = TextAlign.Center
                                )
                            )
                            AppTextField(
                                value = code3,
                                onValueChange = { code ->
                                    if (code.length < 2) code3 = code
                                },
                                modifier = Modifier
                                    .padding(end = 16.dp)
                                    .size(48.dp),
                                contentPadding = PaddingValues(10.dp),
                                textStyle = TextStyle(
                                    fontSize = 20.sp,
                                    textAlign = TextAlign.Center
                                )
                            )
                            AppTextField(
                                value = code4,
                                onValueChange = { code ->
                                    if (code.length < 2) code4 = code
                                    if (code.length == 1) {
                                        viewModel.signIn(email!!, "${code1}${code2}${code3}${code4}")
                                    }
                                },
                                modifier = Modifier
                                    .size(48.dp),
                                contentPadding = PaddingValues(10.dp),
                                textStyle = TextStyle(
                                    fontSize = 20.sp,
                                    textAlign = TextAlign.Center
                                )
                            )
                        }
                        Spacer(modifier = Modifier.height(16.dp))
                        Text(
                            text = "Отправить код повторно можно будет через $timer секунд",
                            fontSize = 15.sp,
                            color = captionColor,
                            modifier = Modifier
                                .widthIn(max = 220.dp),
                            textAlign = TextAlign.Center
                        )
                    }
                }

            }
        }

        if (isLoading) {
            LoadingDialog()
        }

        if (isErrorVisible) {
            AlertDialog(
                onDismissRequest = { isErrorVisible = false },
                title = {
                    Text(text = "Ошибка", fontWeight = FontWeight.SemiBold, fontSize = 18.sp)
                },
                text = {
                    Text(text = errorMessage.toString(), fontSize = 16.sp)
                },
                buttons = {
                    AppTextButton(
                        onClick = { isErrorVisible = false },
                        label = "Ок"
                    )
                }
            )
        }
    }
}